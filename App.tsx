import { useState } from 'react';
import { Text, View, Image, ScrollView, StatusBar } from 'react-native';
import tw from 'twrnc';
import SocialMedia from './pages/component/SocialMedia';
import Project from './pages/component/Project';

export default function App() {
  const [state, setState] = useState({
    name: "Muhammad Fahmi Ady Susilo",
    image: require('./assets/Untitled.png'),
    imageIG: require('./assets/iconSM/instagram.png'),
    imageLI: require('./assets/iconSM/linkedin.png'),
    imageGH: require('./assets/iconSM/github.png'),
    imageCal: require('./assets/calculator/calculator.png'),
    imageCD: require('./assets/codein/detailforum.png'),
    imageLS: require('./assets/lapetshop/pet3.png'),
    imageSG: require('./assets/snakegame/gameSnake.png'),
  })
  return (
    <View style={tw`bg-white max-w-7xl w-full h-full`}>
      <View style={tw`flex items-center justify-between flex-row-reverse py-3 px-6`}>
        <Image
          style={tw`w-10 h-10 rounded-3xl shadow-md border-2 border-black`}
          source={state.image}
        />
        <Text style={tw`text-base font-bold`}>
          {state.name}
        </Text>
      </View>
      <ScrollView style={tw`bg-slate-200`}>
        <View style={tw`py-3 px-6`}>
          <Text style={tw`font-semibold`}>Project</Text>
          <Project image={state.imageCal} colorBG="#1abc9c" name="Calculator" describe="CodeIn is a platform created to make it easy for Indonesian programmers to interact to solve problems or provide useful information for other programmers." />
          <Project image={state.imageCD} colorBG="#1abc9c" name="Codein" describe="CodeIn is a platform created to make it easy for Indonesian programmers to interact to solve problems or provide useful information for other programmers." />
          <Project image={state.imageLS} colorBG="#1abc9c" name="LapetShop" describe="Lapetshop is a website for buying and selling animal needs from food, medicine, toys, accessories and care, this system uses laravel in the making." />
          <Project image={state.imageSG} colorBG="#1abc9c" name="SnakeGame" describe="The snake game that I have made is a browser game, there are already levels, Level and Life Challenges, so it is fun to play" />
        </View>
        <View style={tw`pt-3 pb-14 px-6`}>
          <Text style={tw`font-semibold`}>Social Media</Text>
          <SocialMedia name="@fahmiady_11" image={state.imageIG} colorBG="#e74c3c" link="https://www.instagram.com/fahmiady_11/" />
          <SocialMedia name="Muhammad Fahmi Ady Susilo" image={state.imageLI} colorBG="#8e44ad" link="https://www.linkedin.com/in/muhammad-fahmi-ady-susilo-ba05a41b0/" />
          <SocialMedia name="asfahmi5@gmail.com" image={state.imageGH} colorBG="#2980b9" link="https://github.com/Fahmiady11" />
        </View>
      </ScrollView>
    </View>
  );
}


