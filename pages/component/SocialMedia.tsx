import { Text, View, Image, Linking, TouchableOpacity } from 'react-native';
import tw from 'twrnc';

export default function App(props: any) {

    return (
        <View
            style={tw`flex items-center justify-between flex-col p-3 bg-[${props.colorBG}] border-2  rounded-2xl mt-6 shadow-md`}>
            <Image style={tw`w-8 h-8 shadow-md`}
                source={props.image} />
            <Text style={tw`text-base font-bold mt-2`}>
                {props.name}
            </Text>
            <TouchableOpacity
                style={tw`p-2 bg-blue-500 rounded-md mt-2 shadow-md`}
                onPress={() => Linking.openURL(props.link)}
            >
                <Text style={tw`text-white text-xs`}>Visit Link</Text>
            </TouchableOpacity>
        </View>
    );
}


