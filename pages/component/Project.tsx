import { Text, View, Image, Linking, TouchableOpacity } from 'react-native';
import tw from 'twrnc';

export default function App(props: any) {

    return (
        <View
            style={tw`flex items-center justify-between flex-col p-3 bg-[${props.colorBG}] border-2  rounded-2xl my-3 py-4 shadow-md`}>
            <Image style={tw`w-full h-40 rounded-xl shadow-md object- border-2 border-slate-700`}
                source={props.image} />
            <Text style={tw`text-base font-bold mt-4`}>
                {props.name}
            </Text>
            <Text style={tw`text-xs font-bold mt-2 text-center text-white`}>
                {props.describe}
            </Text>
        </View>
    );
}


